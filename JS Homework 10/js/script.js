"use strict"
window.onload = function() {
    const elements = document.querySelectorAll('[type="password"]');

    elements.forEach(function(elem) {
        elem.parentNode.querySelector('i').addEventListener('click', function() {
            if (elem.type === "password") {
                elem.type = "text";
                this.className = 'fa fa-eye-slash';
            } else {
                elem.type = "password";
                this.className = 'fa fa-eye';
            }
        });
    });
    const button = document.querySelector('.btn');
    button.addEventListener("click", checkValue);

    function checkValue() {

        if (document.getElementById("password").value == document.getElementById("confirm_password").value) {
            alert(`You are welcome`)
        } else {
            const p = document.createElement("p");
            button.before(p);
            p.textContent = "Нужно ввести одинаковые значения";
            p.style.color = "red";
            return;
        };
        button.removeEventListener("click", checkValue);

    }
}