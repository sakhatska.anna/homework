import React from "react";
import Card from "../components/Card/Card.jsx";
import "./Basket.scss";

const Basket = ({ products, saveProd, openModal, toFavorite, favorites }) => {
    const cart = JSON.parse(localStorage.getItem("Cart"));
  const keys = Object.keys(cart);
  const render = products.filter((product) => {
    return keys.includes(product.article.toString());
  });

  return (
    <div className="container">
      {render.map((product) => {
        return (
          <Card
            favorite={favorites.includes(product.article)}
            key={product.article}
            product={product}
            saveProd={saveProd}
            openModal={openModal}
            toFavorite={toFavorite}
            fromBasket
          />
        );
      })}
    </div>
  );
};

export default Basket;
