import React from "react";
import Card from "../components/Card/Card.jsx";
import "../App.scss";

function Favorite({
  products,
  modalAction,
  saveProd,
  openModal,
  toFavorite,
  favorites,
}) {
  return (
    <div className="container">
      {products
        .filter((product) => favorites.includes(product.article))
        .map((product) => {
          return (
            <Card
              favorite
              modalAction={modalAction}
              key={product.article}
              product={product}
              saveProd={saveProd}
              openModal={openModal}
              toFavorite={toFavorite}
              toBasket
            />
          );
        })}
    </div>
  );
}
export default Favorite;
