import React from "react";
import Card from "../components/Card/Card.jsx";
import "./Main.scss";

const Main = ({
  products,
  saveProd,
  openModal,
  toFavorite,
  modalAction,
  favorites,
}) => {
  return (
    <div className="container">
      {products.map((product) => {
        return (
          <Card
            favorite={favorites.includes(product.article)}
            modalAction={modalAction}
            key={product.article}
            product={product}
            saveProd={saveProd}
            openModal={openModal}
            toFavorite={toFavorite}
            toBasket
          />
        );
      })}
    </div>
  );
};

export default Main;
