import React from "react";
import { Route, Redirect, Switch } from "react-router-dom";
import Main from "./Main.jsx";
import Basket from "./Basket.jsx";
import Favorite from "./Favorite.jsx";

const Routes = ({
  products,
  saveProd,
  deleteThisItem,
  openModal,
  toFavorite,
  modalAction,
  favorites,
}) => {
  return (
    <>
      <Switch>
        <Redirect exact from="/" to="/main" />
        <Route
          exact
          path="/main"
          render={() => {
            return (
              <Main
                modalAction={modalAction}
                products={products}
                favorites={favorites}
                saveProd={saveProd}
                openModal={openModal}
                toFavorite={toFavorite}
                toBasket
              />
            );
          }}
        />
        <Route
          exact
          path="/basket"
          render={() => (
            <Basket
              modalAction={modalAction}
              products={products}
              favorites={favorites}
              saveProd={saveProd}
              deleteThisItem={deleteThisItem}
              openModal={openModal}
              toFavorite={toFavorite}
            />
          )}
        />
        <Route
          exact
          path="/favorite"
          render={() => (
            <Favorite
              products={products}
              favorites={favorites}
              modalAction={modalAction}
              saveProd={saveProd}
              openModal={openModal}
              toFavorite={toFavorite}
            />
          )}
        />
      </Switch>
    </>
  );
};

export default Routes;
