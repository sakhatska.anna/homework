import React from "react";
import PropTypes from "prop-types";
import Button from "../Button/Button.jsx";
import { like } from "./like.js";
import "../../App.scss";

const Card = (props) => {
  const {
    product,
    saveProd,
    openModal,
    toFavorite,
    favorite,
    toBasket,
    fromBasket,
  } = props;
  const { name, price, path, article, color } = product;
  return (
    <li className="prod_item">
      <img src={path} className="prod_img" alt="" />
      <h3 className="prod_name">{name}</h3>

      <span className="prod_price"> Price : {price}</span>

      <div className="prod_info">
        {toBasket && (
          <Button
            onClick={() => {
              saveProd(product);
              openModal("Add to Cart");
            }}
            text="Add to Cart"
          />
        )}
        {fromBasket && (
          <Button
            onClick={() => {
              saveProd(product);
              openModal("Remove from cart");
            }}
            text={"Remove from cart"}
          />
        )}
        <span className="prod_artc"> Color : {color}</span>
        <span className="prod_artc"> Article: {article} </span>

        <span onClick={() => toFavorite(article)} className="prod_icon">
          {like(favorite)}
        </span>
      </div>
    </li>
  );
};

Card.propTypes = {
  product: PropTypes.shape({
    name: PropTypes.string,
    article: PropTypes.string,
    price: PropTypes.string,
    path: PropTypes.string,
    color: PropTypes.string,
  }),
};
export default Card;
