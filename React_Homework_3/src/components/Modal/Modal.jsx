import React from "react";
import PropTypes from "prop-types";
import "./Modal.scss";

const Modal = (props) => {
  let { header, closeButton, closeButtonOnClick, actions } = props;

  return (
    <div className={"section__modal"} onClick={closeButtonOnClick}>
      <div className="modal" onClick={(e) => e.stopPropagation()}>
        <header className="modal__header">
          <div>{header} ?</div>
          {closeButton && (
            <div onClick={closeButtonOnClick} className="modal__close">
              X
            </div>
          )}
        </header>
        <div className="modal__button">{actions}</div>
      </div>
    </div>
  );
};
export default Modal;
Modal.propTypes = {
  header: PropTypes.string,
  closeButton: PropTypes.bool,
  text: PropTypes.string,
  actions: PropTypes.array,
};
