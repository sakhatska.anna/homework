import React from "react";
import PropTypes from "prop-types";

const Button = ({ backgroundColor, text, onClick, className }) => {
  return (
    <button
      className={"button"}
      style={{ backgroundColor: backgroundColor }}
      onClick={onClick}
    >
      {text}
    </button>
  );
};

export default Button;

Button.propTypes = {
  text: PropTypes.string,
  onClick: PropTypes.func,
  className: PropTypes.string,
};
