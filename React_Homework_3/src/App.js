import React, { useEffect, useState } from "react";
import "./App.scss";
import axios from "axios";
import Button from "./components/Button/Button.jsx";
import Modal from "./components/Modal/Modal.jsx";
import Header from "./components/Header/Header.jsx";
import Routes from "./routes/Routes.jsx";

const fromFavorite = () => {
  const lisFavorite = localStorage.getItem("Favorite");
  return lisFavorite ? JSON.parse(lisFavorite) : [];
};

const App = () => {
  const [products, setItems] = useState([]);
  const [thisItem, setThisItem] = useState({});
  const [showModal, setShowModal] = useState(false);
  const [favorites, setFavorites] = useState(fromFavorite());
  const [modalAction, setModalAction] = useState("Add to Cart");

  const saveProd = (product) => {
    setThisItem(product);
  };

  const deleteThisItem = (product) => {
    const cart = JSON.parse(localStorage.getItem("Cart"));
    delete cart[product.article];

    if (Object.keys(cart).length === 0) {
      localStorage.removeItem("Cart");
    } else {
      localStorage.setItem("Cart", JSON.stringify(cart));
    }
    closeModal();
  };
  const openModal = (action) => {
    if (action === "Add to Cart") {
      setModalAction("Add to Cart");
    }
    if (action === "Remove from cart") {
      setModalAction("Remove from cart");
    }
    setShowModal(true);
  };

  const closeModal = () => {
    setShowModal(false);
  };

  const addToCart = (article) => {
    if (localStorage.getItem("Cart")) {
      const cart = JSON.parse(localStorage.getItem("Cart"));

      if (Object.keys(cart).includes(article)) {
        cart[article]++;
      } else {
        cart[article] = 1;
      }

      localStorage.setItem("Cart", JSON.stringify(cart));
    } else {
      const cart = {};
      cart[article] = 1;
      localStorage.setItem("Cart", JSON.stringify(cart));
    }
    closeModal();
  };
  const toFavorite = (article) => {
    let newFavorites;
    if (favorites.includes(article)) {
      newFavorites = favorites.filter((favorite) => {
        return favorite !== article;
      });
    } else {
      newFavorites = [...favorites, article];
    }
    setFavorites(newFavorites);
    localStorage.setItem("Favorite", JSON.stringify(newFavorites));
  };
  useEffect(() => {
    axios("./products.json").then((res) => {
      res.data.forEach((card) => {
        card.favorite =
          localStorage.getItem("Favorite") &&
          JSON.parse(localStorage.getItem("Favorite")).includes(card.article);
      });
      setItems(res.data);
    });
  }, []);

  return (
    <div className="App">
      {showModal && (
        <Modal
          closeButton={true}
          closeButtonOnClick={closeModal}
          header={`${modalAction} '${thisItem.name}'`}
          actions={[
            <Button
              key={1}
              onClick={() => {
                if (modalAction === "Add to Cart") {
                  addToCart(thisItem.article);
                }
                if (modalAction === "Remove from cart") {
                  deleteThisItem(thisItem);
                }
              }}
              className="modal-button"
              text="Yes"
            />,
            <Button
              key={2}
              onClick={closeModal}
              className="modal-button"
              text="Cancel"
            />,
          ]}
        />
      )}
      <Header></Header>
      <Routes
        products={products}
        favorites={favorites}
        saveProd={saveProd}
        deleteThisItem={deleteThisItem}
        openModal={openModal}
        toFavorite={toFavorite}
        modalAction={modalAction}
      />
    </div>
  );
};

export default App;
