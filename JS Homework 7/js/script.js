// Document Object Model (DOM) - объектная модель документа , которая показывает все что есть на странице браузера в виде объектов, которые можно менять 
"use strict";

function printArray(array, elem) {

    if (elem != null) {
        parent = elem;
    };
    const parent = document.createElement("ul");
    document.body.appendChild(parent);
    for (let index = 0; index < array.length; index++) {
        let listItem = document.createElement("li");
        listItem.textContent = array[index];
        parent.append(listItem);
    };
}
printArray(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"]);