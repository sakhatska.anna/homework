document.addEventListener("click", function(evnt) {
    const elem = event.srcElement;

    if (elem.hasAttribute("href")) {
        const id_to_show = elem.href.split('#')[1];

        if (id_to_show != null) {
            const tablist = document.getElementsByClassName("tabs-list");

            for (i = 0; i < tablist.length; i++)
                tablist[i].className = tablist[i].className.replace(" tabs-list-show", "");
            document.getElementById(id_to_show).className += " tabs-list-show";
        }
    };
});