// прототипное наследование не использует классы, объекты создаются из других объектов

"use strict";

class Employee {
    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }
    get name() {
        return this._name;
    }
    set name(name) {
        this._name = name;
    }
    get age() {
        return this._age;
    }
    set age(age) {
        this._age = age;
    }
    get salary() {
        return this._salary;
    }
    set salary(salary) {
        this._salary = salary;
    }
};

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    }
    get lang() {
        return this.lang;
    }
    set lang(lang) {
        this._lang = lang;
    }
    get salary() {
        return this._salary;
    }
    set salary(salary) {
        this._salary = salary * 3;
    }
}

const harry = new Employee("Harry", 41, 15689, "English");
console.log(harry);
const draco = new Programmer("Draco", 39, 3459, ["English", "French", ]);
console.log(draco);
const ron = new Programmer("Ron", 25, 14583, "English");
console.log(ron);
const hermione = new Programmer("Hermione", 28, 6375, ["English", "German"]);
console.log(hermione);