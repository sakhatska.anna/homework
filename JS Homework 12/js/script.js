//1. setTimeout позволяет вызвать функцию один раз через определённый интервал времени.setInterval позволяет вызывать функцию регулярно, повторяя вызов через определённый интервал времени. 
// 2.Если в функцию`setTimeout()` передать нулевую задержку вызов функции будет запланирован сразу после выполнения текущего кода.
// 3. clearInterval() очищает от побочных эффектов в виде внешнего лексического окружения, которое может занимать больше памяти, чем сама функция

"use strict"
let slideIndex = 0;
let timeId = setInterval(showSlides, 3000);

showSlides();

function showSlides() {
    let slides = document.getElementsByTagName("img");
    for (let i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
    }
    slideIndex++;
    if (slideIndex > slides.length) {
        slideIndex = 1;
    }
    slides[slideIndex - 1].style.display = "block";
};

timeTest()

function timeTest() {

    const stp = document.getElementById("stop");

    stp.addEventListener("click", pauseTime);

    function pauseTime() {
        clearInterval(timeId);
    }

    const cnt = document.getElementById("continue");
    cnt.addEventListener("click", playTime);

    function playTime() {
        timeId = setInterval(showSlides, 3000);
    };
    // clearInterval(timeId);
}