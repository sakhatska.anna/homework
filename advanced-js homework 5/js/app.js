// асинхронность в Javascript позволяет выполнять задачи не в линейном порядке.

"use strict";

const button = document.getElementById("button");
button.addEventListener("click", () => {
    findIp()
        .then((data) => {
            showIp(data.ip)
                .then(({ query, country, region, city }) => {
                    const div = document.getElementById("div");

                    const IP = document.createElement("p");
                    IP.textContent = `IP:${query}`

                    const countrY = document.createElement("p");
                    countrY.textContent = `Country:${country}`;

                    const regioN = document.createElement("p");
                    regioN.textContent = `Region:${region}`;

                    const citY = document.createElement("p");
                    citY.textContent = `City:${city}`;

                    div.append(IP, countrY, regioN, citY)
                })
        });
});

async function findIp() {
    return await fetch('https://api.ipify.org/?format=json')
        .then((data) => {
            return data.json();
        })
        .catch((err) => {
            console.log(err);
        });

}
async function showIp(ip) {
    return await fetch("http://ip-api.com/json/" + ip)
        .then((data) => {
            return data.json();

        })
        .catch((err) => {
            console.log(err);
        });
}