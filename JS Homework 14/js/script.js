"use strict"

$(document).ready(function() {
    $("#menu").on("click", "a", function(event) {
        event.preventDefault();
        let id = $(this).attr('href');
        let top = $(id).offset().top;
        $('body,html').animate({ scrollTop: top }, 1500);
    });
});


let btn = $('#button');

$(window).scroll(function() {
    if ($(window).scrollTop() > 300) {
        btn.addClass('show');
    } else {
        btn.removeClass('show');
    }
});

btn.on('click', function(e) {
    e.preventDefault();
    $('html, body').animate({ scrollTop: 0 }, '300');
});


$(document).ready(function() {
    $("#slide").click(function() {
        $("#hide").slideToggle();
    });
});