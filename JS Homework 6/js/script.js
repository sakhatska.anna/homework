// forEach используют для перебора массива.Работает он таким способом: он выполняет указанную в нём функцию один раз для каждого элемента в массиве.

"use strict";

function filterBy(array = ['hello', 'world', 23, '23', null], type = 'string') {
    return array.reduce((arr, currentItem) => {
        if (typeof currentItem != type) {
            arr.push(currentItem);
        }
        return arr
    }, []);
}
console.log(filterBy());