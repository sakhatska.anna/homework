// Циклы необходимы для многократного исполнения однотипного задания в коде.
"use strict";
let maxValue = +prompt("Enter your number");
if (maxValue < 0) {
    console.log("Sorry no number");
}

for (let i = 0; i <= maxValue; i++) {
    if (i % 5 === 0) {
        console.log(i);
    }
}