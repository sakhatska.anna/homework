import React from "react";
import "./UserForm.scss";
import { Form, Formik, Field } from "formik";
import * as Yup from "yup";

import UserInput from "./UserInput.jsx";
import { useDispatch } from "react-redux";
import { getBasket, deleteBasket } from "../../store/operation.jsx";
import { SUBMIT_FORM } from "../../store/type";

const validVar = Yup.object().shape({
  userName: Yup.string().required("Name is required"),
  userSurname: Yup.string().required("Surname is required"),
  userAge: Yup.number().required("Age is required"),
  userAddress: Yup.string().required("Address is required"),
  userPhone: Yup.number().required("Phone is required"),
});

const UserForm = () => {
  const dispatch = useDispatch();

  const setUser = (userInfo, basket) => {
    dispatch({
      type: SUBMIT_FORM,
      payload: { userInfo: userInfo, basket: basket },
    });
  };
  const submitForm = (
    { userName, userSurname, userAge, userAddress, userPhone },
    { resetForm }
  ) => {
    const userInfo = {
      userName: userName,
      userSurname: userSurname,
      userAge: userAge,
      userAddress: userAddress,
      userPhone: userPhone,
    };
    const basket = getBasket();

    setUser(userInfo, basket);
    deleteBasket();
    resetForm({});

    console.log("Basket:", basket);
    console.log("UserInfo:", userInfo);
  };
  return (
    <div className="form">
      <div className="form__name">
        <p>Enter data for delivery</p>
      </div>

      <Formik
        initialValues={{
          userName: "",
          userSurname: "",
          userAge: "",
          userAddress: "",
          userPhone: "",
        }}
        onSubmit={submitForm}
        validationSchema={validVar}
        validateOnChange={false}
        validateOnBlur={false}
      >
        {() => {
          return (
            <div>
              <Form>
                <Field
                  name="userName"
                  type="text"
                  placeholder="Name"
                  component={UserInput}
                />
                <Field
                  name="userSurname"
                  type="text"
                  placeholder="Surname"
                  component={UserInput}
                />
                <Field
                  name="userAge"
                  type="number"
                  placeholder="Age"
                  component={UserInput}
                />
                <Field
                  name="userAddress"
                  type="text"
                  placeholder="Order address"
                  component={UserInput}
                />
                <Field
                  name="userPhone"
                  type="number"
                  placeholder="Phone number"
                  component={UserInput}
                />
                <button className="form__submit" type="submit">
                  Checkout
                </button>
              </Form>
            </div>
          );
        }}
      </Formik>
    </div>
  );
};
export default UserForm;
