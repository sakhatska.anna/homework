import { render, fireEvent } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import Modal from "./Modal.jsx";

jest.mock("react-redux", () => ({
  useSelector: () => {
    return { info: "testAction", data: { name: "itemName" } };
  },
}));

const modalId = "modalJest";
const buttonId = "closeButtonJest";

describe("Modal.js", () => {
  test("Render", () => {
    render(<Modal />);
  });

  test("Action", () => {
    const action = ["Action"];
    render(<Modal actions={action} />);
  });
  test("Close Modal", () => {
    const closeModal = jest.fn();
    const { getByTestId } = render(<Modal closeModal={closeModal} />);
    userEvent.click(getByTestId(modalId));
    expect(closeModal).toHaveBeenCalled();
  });
  test("Close Button", () => {
    const closeModal = jest.fn();
    const { getByTestId } = render(
      <Modal closeModal={closeModal} closeButton />
    );
    userEvent.click(getByTestId(buttonId));
    expect(closeModal).toHaveBeenCalled();
  });
  test("Class", () => {
    const className = "modal-background";
    const { getByTestId } = render(<Modal className={className} />);
    expect(getByTestId(modalId)).toHaveClass(className);
  });

  test("stopPropagation", () => {
    const { getByTestId } = render(<Modal />);
    const bodyMod = getByTestId("modalJest");
    expect(bodyMod).toBeInTheDocument();
    fireEvent.click(bodyMod);
  });

  test("Snapshot", () => {
    const { container } = render(<Modal />);
    expect(container.innerHTML).toMatchSnapshot();
  });
});
