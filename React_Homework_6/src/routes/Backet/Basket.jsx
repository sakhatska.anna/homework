import React from "react";
import Card from "../../components/Card/Card.jsx";
import "./Basket.scss";
import { useSelector } from "react-redux";
import UserForm from "../../components/User/UserForm.jsx";

const Basket = () => {
  const products = useSelector((state) => state.products.data);
  const basket = products.filter((product) => product.inBasketAmount);

  return (
    <div className="container">
      <UserForm />
      {basket.map((product) => {
        return <Card key={product.article} product={product} fromBasket />;
      })}
    </div>
  );
};

export default Basket;
