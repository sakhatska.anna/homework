// Var, let и const это переменные, которые определяют, какие значения в них хранятся. Как в математике младших классов, когда мы пишем «Х», которое может иметь разные значения. Let для тех значений которые будут меняться в дальнейшем, const –  это, как понятно с названия константа, то есть значение которое до окончания программы останется НЕИЗМЕННЫМ. Объявлять переменную через var считается плохим тоном, потому что в современном коде использование только let и const позволяет читать код легче и быстрее, также это обусловлено тем, что раньше var использовался для поддерживания определенных функций, от которых сейчас отходят.


"use strict";
const userName = prompt("Enter your name");
console.log(userName);
if (typeof userName === "string") {
    console.log("User Name is ok");
} else(console.log("User Name is not ok"));

const userAge = +prompt("How old are you?");
console.log(userAge);
if (typeof userAge === "number") {
    console.log("User Age is ok");
} else(console.log("User Age is not ok"));
if (userAge < 18) {
    alert("You are not allowed to visit this website");
} else if (userAge >= 18 && userAge <= 22) {

    if (confirm("Are you sure you want to continue?")) {
        alert(`Welcome, ${userName}`);
    } else {
        alert("You are not allowed to visit this website");
    }
} else if (userAge > 22) {
    alert(`Welcome, ${userName}`);
};