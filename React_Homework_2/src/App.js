import React, { Component } from "react";
import "./App.scss";
import Button from "./components/Button.jsx";
import Modal from "./components/Modal.jsx";
import Product from "./components/Products.jsx";
import List from "./components/List.jsx";

class App extends Component {
  state = {
    products: [],
    isOpen: false,
    cardId: null,
  };

  editModal = (article) => {
    if (article) {
      this.setState({
        ...this.state,
        isOpen: !this.state.isOpen,
        cardId: article,
      });
    } else {
      this.setState({
        ...this.state,
        isOpen: !this.state.isOpen,
        cardId: null,
      });
    }
  };

  shopCart = (cart) => {
    this.setState({
      ...this.state,
      isOpen: !this.state.isOpen,
    });
    const cartInfo = JSON.parse(localStorage.getItem("prodInfo"));
    const product = this.state.products.find(
      (product) => product.article === cart
    );
    cartInfo.push(product);
    localStorage.setItem("prodInfo", JSON.stringify(cartInfo));
  };
  componentDidMount() {
    fetch(`products.json`)
      .then((response) => {
        return response.json();
      })
      .then((products) => {
        this.setState({
          products: products.products,
        });
      });
    if (localStorage.length === 0) {
      localStorage.setItem("prodInfo", JSON.stringify([]));
    }
  }
  render() {
    let prods = this.state.products;
    return (
      <React.Fragment>
        {this.state.isOpen && (
          <Modal
            header={"Do you want to buy this Beaty?"}
            text={"It will be the most pleasant purchase in your life :)"}
            closeButton={true}
            closeModal={this.editModal}
            actions={
              <>
                <Button
                  openModal={() => this.shopCart(this.state.cardId)}
                  text={"Sure"}
                />
                <Button openModal={this.editModal} text={"Not Today"} />
              </>
            }
          />
        )}
        <div
        // className="main_content"
        >
          <List
            content={prods.map((item, key) => {
              return (
                <Product
                  key={key}
                  title={item.title}
                  price={item.price}
                  img={item.url}
                  article={item.article}
                  openModal={() => this.editModal(item.article)}
                  color={item.color}
                />
              );
            })}
          />
        </div>
      </React.Fragment>
    );
  }
}

export default App;
