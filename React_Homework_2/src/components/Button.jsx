import React, { Component } from "react";
import PropTypes from "prop-types";
import "../App.scss";

class Button extends Component {
  render() {
    let { text, color, openModal } = this.props;
    return (
      <button
        className="button"
        onClick={openModal}
        style={{ backgroundColor: color }}
      >
        {text}
      </button>
    );
  }
}

Button.propTypes = {
  text: PropTypes.string,
  color: PropTypes.string,
  openModal: () => {},
  closeModal: () => {},
  actions: () => {},
};
Button.defaultProps = {
  text: "Add to Cart",
};
export default Button;
