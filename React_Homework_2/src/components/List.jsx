import React, { Component } from "react";
import "../App.scss";

export class List extends Component {
  render() {
    const { content } = this.props;
    return <ul className="prod_list">{content}</ul>;
  }
}
List.propTypes = {
  content: () => {},
};
export default List;
