// Экранирование – это возможность использовать специальные символы (которые мы обычно используем при написании кода) как обычные (например, в тексте) 
"use strict";

function createNewUser() {
    let newUser = {
        firstName: prompt("Enter your first name"),
        lastName: prompt("Enter your last name"),
        birthday: prompt("Enter your date of birth", 'dd.mm.yyyy'),
        getLogin() {
            let login = this.firstName[0] + this.lastName;
            return login.toLowerCase();
        },
        getAge() {
            let age = new Date().getFullYear() - this.birthday.substr(6, 4);
            return age;


        },
        getPassword() {
            let password = this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.substr(6, 4);
            return password;
        },
    };
    return newUser;
}
const result = createNewUser();
console.log(result.getLogin());
console.log(result.getAge());
console.log(result.getPassword());