//Для корректной роботы input необходимо использовать специальное событие input , потому что одних событий клавиатуры недостаточно. 

"use strict"

document.addEventListener("keydown", function(event) {
    const ent = document.getElementById("enter");
    if (event.code == "Enter") {
        ent.style.backgroundColor = "blue";
    } else(ent.style.backgroundColor = "black");

    const s = document.getElementById("s");
    if (event.code == "KeyS") {
        s.style.backgroundColor = "blue";
    } else(s.style.backgroundColor = "black");

    const e = document.getElementById("e");
    if (event.code == "KeyE") {
        e.style.backgroundColor = "blue";
    } else(e.style.backgroundColor = "black");

    const o = document.getElementById("o");
    if (event.code == "KeyO") {
        o.style.backgroundColor = "blue";
    } else(o.style.backgroundColor = "black");

    const n = document.getElementById("n");
    if (event.code == "KeyN") {
        n.style.backgroundColor = "blue";
    } else(n.style.backgroundColor = "black");

    const l = document.getElementById("l");
    if (event.code == "KeyL") {
        l.style.backgroundColor = "blue";
    } else(l.style.backgroundColor = "black");

    const z = document.getElementById("z");
    if (event.code == "KeyZ") {
        z.style.backgroundColor = "blue";
    } else(z.style.backgroundColor = "black");

});