import React from "react";
import { NavLink } from "react-router-dom";
import "./Header.scss";

const Header = () => {
  return (
    <div className="header">
      <NavLink to="/main" className="header__link">
        Home
      </NavLink>
      <NavLink to="/favorite" className="header__link">
        Favorite
      </NavLink>
      <NavLink to="/basket" className="header__link">
        Basket
      </NavLink>
    </div>
  );
};

export default Header;
