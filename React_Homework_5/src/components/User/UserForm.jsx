import React from "react";
import "./UserForm.scss";
import { Form, Formik, Field } from "formik";
import * as Yup from "yup";

import UserInput from "./UserInput.jsx";
import { useDispatch } from "react-redux";
import { getBasket, deleteBasket } from "../../store/operation.jsx";
import { SUBMIT_FORM } from "../../store/type";

const validVar = Yup.object().shape({
  userName: Yup.string().required("Your name is very important for us ;)"),
  userSurname: Yup.string().required(
    "Your Surname is very important for us ;)"
  ),
  userAge: Yup.number().required("Your age is very important for us ;)"),
  userPN: Yup.number().required(
    "Your phone number is very important for us ;)"
  ),
  userAddress: Yup.string().required(
    "Your address is very important for us ;)"
  ),
});

const UserForm = () => {
  const dispatch = useDispatch();

  const setUser = (userInfo, basket) => {
    dispatch({
      type: SUBMIT_FORM,
      payload: { userInfo: userInfo, basket: basket },
    });
  };
  const submitForm = (
    { userName, userSurname, userAge, userAddress, userPN },
    { resetForm }
  ) => {
    const userInfo = {
      userName: userName,
      userSurname: userSurname,
      userAge: userAge,
      userAddress: userAddress,
      userPN: userPN,
    };
    const basket = getBasket();

    setUser(userInfo, basket);
    deleteBasket();
    resetForm({});

    console.log("Basket:", basket);
    console.log("UserInfo:", userInfo);
  };
  return (
    <div className="form">
      <div className="form__name">
        <p>Enter data for delivery</p>
      </div>

      <Formik
        initialValues={{
          userName: "",
          userSurname: "",
          userAge: "",
          userAddress: "",
          userPN: "",
        }}
        onSubmit={submitForm}
        validationSchema={validVar}
        validateOnChange={false}
        validateOnBlur={false}
      >
        {() => {
          return (
            <div>
              <Form>
                <Field
                  name="userName"
                  type="text"
                  placeholder="Name"
                  component={UserInput}
                />
                <Field
                  name="userSurname"
                  type="text"
                  placeholder="Surname"
                  component={UserInput}
                />
                <Field
                  name="userAge"
                  type="number"
                  placeholder="Age"
                  component={UserInput}
                />
                <Field
                  name="userAddress"
                  type="text"
                  placeholder="Order address"
                  component={UserInput}
                />
                <Field
                  name="userPN"
                  type="number"
                  placeholder="Phone number"
                  component={UserInput}
                />
                <button className="form__submit" type="submit">
                  Checkout
                </button>
              </Form>
            </div>
          );
        }}
      </Formik>
    </div>
  );
};
export default UserForm;
