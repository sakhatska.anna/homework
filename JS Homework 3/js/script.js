// 1. Функции в программировании необходимы для того чтобы не повторять одну и ту же часть кода, и не нагружать программу.
// 2. В функцию передают аргумент, для того, чтобы использовать его в дальнейшем внутри этой функции.
"use strict";
const firstNummer = +prompt("Enter first number");
const secondNummer = +prompt("Enter second number");
const operator = prompt("Choose the operation: + , - , * , / .");

function createAction() {
    if (operator == "+") {
        return firstNummer + secondNummer;
    } else if (operator == "-") {
        return firstNummer - secondNummer;
    } else if (operator == "*") {
        return firstNummer * secondNummer;
    } else if (operator == "/") {
        return firstNummer / secondNummer;
    }

};
console.log(createAction());