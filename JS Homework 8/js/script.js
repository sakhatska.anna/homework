// обработчик событий инструмент-функция, что позволяет коду реагировать на действия пользователя.
"use strict";
const div = document.getElementById("div")
const price = document.getElementById("price");

price.value = "";

price.addEventListener("mouseover", (e) => {
    e.target.style.borderColor = "#8ceb00";
});
price.addEventListener("mouseout", (e) => {
    e.target.style.borderColor = "";
});
price.addEventListener("blur", getPrice);

function getPrice() {
    const span = document.createElement("span");
    const button = document.createElement("button");
    price.style.color = "#8ceb00";
    span.textContent = `Текущая цена: ${price.value}`;

    div.insertAdjacentElement("beforebegin", span);
    span.insertAdjacentElement("afterend", button);

    button.style.backgroundColor = "white";
    button.style.borderWidth = "1px";
    button.textContent = " X ";

    button.addEventListener("click", removePrice);

    function removePrice() {
        location.reload();
    };

    if (price.value < 0) {
        span.remove();
        button.remove();
        price.style.borderColor = "red";
        price.style.color = "red";
        const h2 = document.createElement("h2");
        h2.textContent = `Please enter correct price`;
        div.insertAdjacentElement("afterend", h2);
        let timerId = setTimeout(() => location.reload(), 3000);
        return timerId;
    }
    price.removeEventListener("blur", getPrice);
};