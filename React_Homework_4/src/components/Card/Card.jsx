import React from "react";
import PropTypes from "prop-types";
import Button from "../Button/Button.jsx";
import { useDispatch } from "react-redux";
import { like } from "./like.js";
import "../../App.scss";
import { SHOW_MODAL, ADD_REMOVE_FAVORITE } from "../../store/type.jsx";

const Card = ({ product, toBasket, fromBasket }) => {
  const { name, price, path, article, color, favorite } = product;

  const dispatch = useDispatch();

  const toFavorite = (id) => {
    dispatch({ type: ADD_REMOVE_FAVORITE, payload: id });
  };
  const showModal = (info, data) => {
    dispatch({ type: SHOW_MODAL, payload: { info, data } });
  };
  return (
    <li className="prod_item">
      <img src={path} className="prod_img" alt="" />
      <h3 className="prod_name">{name}</h3>

      <span className="prod_price"> Price : {price}</span>

      <div className="prod_info">
        {toBasket && (
          <Button
            onClick={() => {
              showModal("Add to Cart", product);
            }}
            text={"Add to Cart"}
          />
        )}
        {fromBasket && (
          <Button
            onClick={() => {
              showModal("Remove from cart", product);
            }}
            text={"Remove from cart"}
          />
        )}
        <span className="prod_artc"> Color : {color}</span>
        <span className="prod_artc"> Article: {article} </span>

        <span onClick={() => toFavorite(article)} className="prod_icon">
          {like(favorite)}
        </span>
      </div>
    </li>
  );
};
export default Card;
Card.propTypes = {
  product: PropTypes.shape({
    name: PropTypes.string,
    article: PropTypes.string,
    price: PropTypes.string,
    path: PropTypes.string,
    color: PropTypes.string,
    favorite: PropTypes.bool,
  }),
  toBasket: PropTypes.bool,
  fromBasket: PropTypes.bool,
};
