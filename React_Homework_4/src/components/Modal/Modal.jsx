import React from "react";
import PropTypes from "prop-types";
import "./Modal.scss";
import { useSelector } from "react-redux";

const Modal = (props) => {
  const { closeButton, closeModal, actions } = props;
  const { info: action } = useSelector((state) => state.modal);

  return (
    <div className={"section__modal"} onClick={closeModal}>
      <div className="modal" onClick={(e) => e.stopPropagation()}>
        <header className="modal__header">
          <div>{action} ?</div>
          {closeButton && (
            <div onClick={closeModal} className="modal__close">
              X
            </div>
          )}
        </header>

        <div className="modal__button">{actions}</div>
      </div>
    </div>
  );
};

Modal.propTypes = {
  closeButton: PropTypes.bool,
  closeModal: PropTypes.func,
  actions: PropTypes.array,
};

export default Modal;
