import React, { useEffect } from "react";
import "./App.scss";

import Button from "./components/Button/Button.jsx";
import Modal from "./components/Modal/Modal.jsx";
import Header from "./components/Header/Header.jsx";
import Routes from "./routes/Routes.jsx";
import { useDispatch, useSelector } from "react-redux";
import { loadItems } from "./store/operation.jsx";
import {
  ADD_TO_BASKET,
  REMOVE_FROM_BASKET,
  CLOSE_MODAL,
} from "./store/type.jsx";

const App = () => {
  const dispatch = useDispatch();
  const isOpen = useSelector((state) => state.modal.isOpen);
  const thisItem = useSelector((state) => state.modal.data);
  const infoModal = useSelector((state) => state.modal.info);

  const closeModal = () => {
    dispatch({ type: CLOSE_MODAL });
  };
  const addToBasket = (data) => {
    dispatch({ type: ADD_TO_BASKET, payload: data });
  };
  const removeFromBasket = (data) => {
    dispatch({ type: REMOVE_FROM_BASKET, payload: data });
  };
  useEffect(() => {
    dispatch(loadItems());
  }, [dispatch]);

  return (
    <div className="App">
      {isOpen && (
        <Modal
          closeButton={true}
          closeModal={closeModal}
          actions={[
            <Button
              key={1}
              onClick={() => {
                if (infoModal === "Add to Cart") {
                  addToBasket(thisItem.article);
                }
                if (infoModal === "Remove from cart") {
                  removeFromBasket(thisItem.article);
                }
                closeModal();
              }}
              className="modal-button"
              text="Yes"
            />,
            <Button
              key={2}
              onClick={closeModal}
              className="modal-button"
              text="Cancel"
            />,
          ]}
        />
      )}
      <Header></Header>
      <Routes />
    </div>
  );
};

export default App;
