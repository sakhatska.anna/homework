import {
  SUCCESS,
  SHOW_MODAL,
  ADD_REMOVE_FAVORITE,
  CLOSE_MODAL,
  ADD_TO_BASKET,
  REMOVE_FROM_BASKET,
} from "./type.jsx";
import {
  getBasket,
  setBasket,
  getFavorite,
  setFavorite,
} from "./operation.jsx";

const initialState = {
  products: {
    data: [],
    isLoading: true,
  },
  modal: {
    info: "",
    data: null,
    isOpen: false,
  },
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SUCCESS:
      return {
        ...state,
        products: { ...state.data, data: action.payload, isLoading: false },
      };
    case SHOW_MODAL:
      return {
        ...state,
        modal: {
          ...state.modal,
          info: action.payload.info,
          data: action.payload.data,
          isOpen: true,
        },
      };
    case CLOSE_MODAL:
      return { ...state, modal: { ...state.modal, isOpen: false } };
    case ADD_REMOVE_FAVORITE:
      const newArray = state.products.data.map((product) => {
        if (product.article === action.payload) {
          product.favorite = !product.favorite;

          const favorites = getFavorite();
          if (favorites.includes(product.article)) {
            favorites.splice(favorites.indexOf(product.article), 1);
          } else {
            favorites.push(product.article);
          }
          setFavorite(favorites);
        }
        return product;
      });
      return { ...state, products: { ...state.products, data: newArray } };
    case ADD_TO_BASKET:
      const newProducts = state.products.data.map((product) => {
        if (product.article === action.payload) {
          product.inBasketAmount++;

          const basket = getBasket();
          if (Object.keys(basket).includes(product.article)) {
            basket[product.article]++;
          } else {
            basket[product.article] = 1;
          }
          setBasket(basket);
        }
        return product;
      });
      return { ...state, products: { ...state.products, data: newProducts } };
    case REMOVE_FROM_BASKET:
      const newProductsS = state.products.data.map((product) => {
        if (product.article === action.payload) {
          product.inBasketAmount--;

          const basket = getBasket();
          basket[product.article]--;
          if (basket[product.article] === 0) {
            delete basket[product.article];
          }
          setBasket(basket);
        }
        return product;
      });
      return {
        ...state,
        product: { ...state.products, data: newProductsS },
      };
    default:
      return state;
  }
};
export default reducer;
