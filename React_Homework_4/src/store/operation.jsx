import axios from "axios";
import { SUCCESS } from "./type.jsx";

export const getBasket = () => {
  const basket = localStorage.getItem("Basket");
  return basket ? JSON.parse(basket) : {};
};

export const setBasket = (data) => {
  localStorage.setItem("Basket", JSON.stringify(data));
};

export const getFavorite = () => {
  const lisFavorite = localStorage.getItem("Favorite");
  return lisFavorite ? JSON.parse(lisFavorite) : [];
};

export const setFavorite = (data) => {
  localStorage.setItem("Favorite", JSON.stringify(data));
};

export const loadItems = () => (dispatch) => {
  const favorites = getFavorite();
  const basket = getBasket();

  axios("./products.json").then((res) => {
    const newProducts = res.data.map((product) => {
      product.favourite = favorites.includes(product.article);
      product.inBasketAmount = basket[product.article] || null;
      return product;
    });
    dispatch({ type: SUCCESS, payload: newProducts });
  });
};
