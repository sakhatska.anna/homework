import React from "react";
import Card from "../components/Card/Card.jsx";
import "./Main.scss";
import { useSelector } from "react-redux";

const Main = () => {
  const products = useSelector((state) => state.products.data);
  return (
    <div className="container">
      {products.map((product) => {
        return <Card key={product.article} product={product} toBasket />;
      })}
    </div>
  );
};

export default Main;
