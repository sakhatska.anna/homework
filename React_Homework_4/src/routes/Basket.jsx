import React from "react";
import Card from "../components/Card/Card.jsx";
import "./Basket.scss";
import { useSelector } from "react-redux";

const Basket = () => {
  const products = useSelector((state) => state.products.data);
  const basket = products.filter((product) => product.inBasketAmount);

  return (
    <div className="container">
      {basket.map((product) => {
        return <Card key={product.article} product={product} fromBasket />;
      })}
    </div>
  );
};

export default Basket;
