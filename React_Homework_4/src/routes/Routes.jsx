import React from "react";
import { Route, Redirect, Switch } from "react-router-dom";
import Main from "./Main.jsx";
import Basket from "./Basket.jsx";
import Favorite from "./Favorite.jsx";

const Routes = () => {
  return (
    <>
      <Switch>
        <Redirect exact from="/" to="/main" />
        <Route exact path="/main" component={Main} />
        <Route exact path="/basket" component={Basket} />
        <Route exact path="/favorite" component={Favorite} />
      </Switch>
    </>
  );
};

export default Routes;
