const { src, dest } = require('gulp');
const imagemin = require('gulp-imagemin');
const pngquant = require('imagemin-pngquant');

const image = () => {
    return src("./src/image/**/*")
        .pipe(imagemin(
                pngquant({ progressive: true }))
            .pipe(dest("./dist/img/")));
};
exports.image = image;