const { src, dest } = require("gulp");
const sass = require("gulp-sass");
const autoprefixer = require("gulp-autoprefixer");
const concat = require("gulp-concat");
const cleanCSS = require("gulp-clean-css");

const styles = () => {
    return src("./src/styles/**/*.scss")
        .pipe(sass({ outputStyle: "compressed" }).on("error", sass.logError))
        .pipe(autoprefixer({ overrideBrowserslist: ['last 10 versions'], grid: true }))
        .pipe(concat('styles.min.css'))
        .pipe(cleanCSS({ compatibility: '*' }))
        .pipe(dest("./dist/styles/"))
};

exports.styles = styles;