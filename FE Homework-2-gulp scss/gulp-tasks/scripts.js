const { src, dest } = require("gulp");
const concat = require("gulp-concat");
const minifyjs = require("gulp-js-minify");

const scripts = () => {
    return src("./src/scripts/*.js")
        .pipe(concat('scripts.min.js'))
        .pipe(minifyjs())
        .pipe(dest("./dist/scripts/"))
};

exports.scripts = scripts;