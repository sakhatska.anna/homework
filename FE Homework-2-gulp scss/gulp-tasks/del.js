const del = require("del");
const clean = () => {
    return del('dist/**/*', { force: true })
};
exports.clean = clean;