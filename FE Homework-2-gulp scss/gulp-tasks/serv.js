const gulp = require("gulp");
const { styles } = require("./styles");
const { scripts } = require("./scripts");
const { image } = require("./image");

const browserSync = require("browser-sync").create();

function reloadBrow(cb) {
    server.reload();
    cb()
}

module.exports = function serv(cb) {
    browserSync.init({
        server: {
            baseDir: "./",
            build: "build"
        },
        browser: "firefox",
        notify: false,
        open: true,
        cors: true
    })
    gulp.watch("src/image/*.{gif,png,jpg,svg,webp}", gulp.series(image, reloadBrow))
    gulp.watch("src/scripts/**/*.js", gulp.series(scripts, reloadBrow)),
        gulp.watch("/index.html").on("change", browserSync.reload),
        gulp.watch("src/styles/**/*.scss",
            gulp.series(styles, cb => gulp.src("./dist/styles").pipe(browserSync.stream()).on('end', cb)))
    return cb()
};