const { series, parallel } = require("gulp");
const { scripts } = require("./gulp-tasks/scripts");
const { clean } = require("./gulp-tasks/del");
const { styles } = require("./gulp-tasks/styles");
const { image } = require("./gulp-tasks/image");
const serv = require("./gulp-tasks/serv");


exports.dev = series(clean, parallel(styles, scripts, image, serv));
exports.build = series(clean, styles, scripts, image);