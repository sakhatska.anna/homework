// Уместно использовать в коде конструкцию `try...catch`, который позволяет обработать произвольные ошибки в блоке кода когда:
// 1) удобнее сделать действие и потом разбираться с результатом
// 2) используемый метод не позволяет проверить формат строки перед разбором, и блок `try...catch`
// становится необходимым.

"use strict";

const books = [{
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];

const bookList = document.createElement("ul");
document.getElementById("root").append(bookList);

const chekedBooks = (book) => {
    const { author, name, price } = book
    if (!author) {
        throw new Error("Книга без автора");
    } else if (!name) {
        throw new Error("Книга без названия");
    } else if (!price) {
        throw new Error("Книга без цены");
    }
};

const rightBooks = (array) => {
    array.forEach(book => {
        const { author, name, price } = book
        try {
            chekedBooks(book)
            const authorUnit = document.createElement("li");
            const nameUnit = document.createElement("p");
            const priceUnit = document.createElement("p");
            authorUnit.textContent = ` Автор: ${author}`;
            nameUnit.textContent = `Название: ${name}`;
            priceUnit.textContent = `Цена: ${price}`;
            bookList.append(authorUnit);
            authorUnit.append(nameUnit, priceUnit);
        } catch (e) {
            console.group("Error");
            console.log(book);
            console.error(e.message);
            console.groupEnd();
        }
    })
};
rightBooks(books);