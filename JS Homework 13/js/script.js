"use strict"
const button = document.getElementsByClassName("button");
const text = document.getElementById("text");
const link = document.getElementsByTagName("a");


const idB = document.getElementById("button");
idB.addEventListener("click", checkColor);

function checkColor() {
    if (document.body.style.color !== "white") {
        changeColor();

    } else {
        document.location.reload();
    };
};

function changeColor() {

    for (let i = 0; i < button.length; i++) {
        button[i].classList.remove("white");
        button[i].classList.add("black");
    }
    text.classList.remove("white-text");
    text.classList.add("black-text");
    for (let i = 0; i < link.length; i++) {
        link[i].classList.remove("white-link");
        link[i].classList.add("black-link");
    }
    document.body.style.backgroundColor = "black";
    document.body.style.color = "white";



}