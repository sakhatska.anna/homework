import React from "react";
import Card from "../../components/Card/Card.jsx";
import "../../App.scss";
import { useSelector } from "react-redux";

function Favorite() {
  const products = useSelector((state) => state.products.data);
  const favorites = products.filter((product) => product.favorite);
  return (
    <div className="container">
      {favorites.map((product) => {
        return <Card key={product.article} product={product} toBasket />;
      })}
    </div>
  );
}
export default Favorite;
