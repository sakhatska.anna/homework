import React from "react";
import { Route, Redirect, Switch } from "react-router-dom";
import Main from "./Main/Main.jsx";
import Basket from "./Backet/Basket.jsx";
import Favorite from "./Favorite/Favorite.jsx";
import Page404 from "./404/404.jsx";

const Routes = () => {
  return (
    <>
      <Switch>
        <Redirect exact from="/" to="/main" />
        <Route exact path="/main" component={Main} />
        <Route exact path="/basket" component={Basket} />
        <Route exact path="/favorite" component={Favorite} />
        <Route path="*" component={Page404} />
      </Switch>
    </>
  );
};

export default Routes;
