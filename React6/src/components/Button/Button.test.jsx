import { render } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import Button from "./Button.jsx";

const button = "button";

describe("Button.jsx", () => {
  test("Render", () => {
    render(<Button />);
  });
  test("Class & text", () => {
    const text = "text";
    const className = "class";
    const { getByTestId, getByText } = render(
      <Button text={text} className={className} />
    );
    expect(getByText(/text/i)).toBeInTheDocument();
    expect(getByTestId(button)).toHaveClass(className);
  });
  test("Snapshot", () => {
    const { container } = render(<Button />);
    expect(container.innerHTML).toMatchSnapshot();
  });

  test("onClick", () => {
    const onClickFn = jest.fn();
    const { getByTestId } = render(<Button onClick={onClickFn} />);
    userEvent.click(getByTestId(button));
    expect(onClickFn).toHaveBeenCalled();
  });
});
