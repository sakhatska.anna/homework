import React from "react";
import "./UserInput.scss";

const UserInput = (props) => {
  const { field, form, placeholder, type } = props;
  const { name } = field;
  return (
    <div>
      {form.errors[name] && form.touched[name] && (
        <div className="input__err">{form.errors[name]}</div>
      )}
      <input placeholder={placeholder} type={type} {...field} />
    </div>
  );
};
export default UserInput;
