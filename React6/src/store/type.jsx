export const SUCCESS = "SUCCESS";
export const SHOW_MODAL = "SHOW_MODAL";
export const ADD_REMOVE_FAVORITE = "ADD_REMOVE_FAVORITE";
export const CLOSE_MODAL = "CLOSE_MODAL";
export const ADD_TO_BASKET = "ADD_TO_BASKET";
export const REMOVE_FROM_BASKET = "REMOVE_FROM_BASKET";
export const SUBMIT_FORM = "SUBMIT_FORM";
