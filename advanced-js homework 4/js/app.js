// AJAX – технология обращения к серверу без перезагрузки страницы. С его помощью страница продолжает работать, даже при отправке запросов на сервер в фоновом режиме. 

"use strict";

const url = 'https://swapi.dev/api/films/';
fetch(url)
    .then((response) => response.json())
    .then((data) => data.results)
    .then(films => {
        return films.forEach(film => {
            const div = document.createElement("div");
            document.body.prepend(div);
            const list = document.createElement("ul");
            const h1 = document.createElement('h1');
            h1.textContent = film.title;
            div.append(h1);
            const h2 = document.createElement('h2');
            h2.textContent = film.episode_id;
            div.append(h2);
            const p = document.createElement('p');
            p.textContent = film.opening_crawl;
            div.append(p);
            div.append(list)
            film.characters.forEach(pers => {
                return fetch(pers)
                    .then(response => response.json())
                    .then(character => {
                        let li = document.createElement("li")
                        li.textContent = character.name;
                        list.append(li);
                    })
            })
        })
    })