import React, { Component } from "react";
import "./App.scss";
import Button from "./components/Button.jsx";
import Modal from "./components/Modal.jsx";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fModal: false,
      sModal: false,
    };
  }
  editFModal = () => {
    this.setState(() => ({
      fModal: !this.state.fModal,
    }));
  };
  editSModal = () => {
    this.setState(() => ({
      sModal: !this.state.sModal,
    }));
  };
  render() {
    return (
      <React.Fragment>
        {this.state.fModal && (
          <Modal
            header={"Do you want to delete this file?"}
            text={
              "Once you delete this file, it won’t be possible to undo this action. Are you sure you want to delete it?"
            }
            closeButton
            closeModal={this.editFModal}
            actions={
              <>
                <Button
                  openModal={this.editFModal}
                  text={"OK"}
                  color={"#b3382c"}
                />
                <Button
                  openModal={this.editFModal}
                  text={"Cancel"}
                  color={"#b3382c"}
                />
              </>
            }
          />
        )}
        {this.state.sModal && (
          <Modal
            header={"Are You sure in your choise?"}
            text={"If you are sure press ' YES ' , otherwise do not press"}
            closeButton
            closeModal={this.editSModal}
            actions={
              <>
                <Button
                  openModal={this.editSModal}
                  text={"YES"}
                  color={"#CC6E66"}
                />
                <Button
                  openModal={this.editSModal}
                  text={"NO"}
                  color={"#CC6E66"}
                />
              </>
            }
          />
        )}
        <div className="content">
          <Button
            openModal={this.editFModal}
            color={"#013C34"}
            text={"Open first modal"}
          />
          <Button
            openModal={this.editSModal}
            color={"#420A05"}
            text={"Open second modal"}
          />
        </div>
      </React.Fragment>
    );
  }
}

export default App;
